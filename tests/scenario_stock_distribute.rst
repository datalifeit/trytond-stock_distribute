================
Stock distribute
================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.exceptions import UserError, UserWarning
    >>> from proteus import config, Model, Wizard
    >>> today = datetime.date.today()

Create database::

    >>> config = config.set_trytond()
    >>> config.pool.test = True

Install stock storage space Module::

    >>> Module = Model.get('ir.module.module')
    >>> module, = Module.find([('name', '=', 'stock_distribute')])
    >>> module.click('install')
    >>> Wizard('ir.module.module.install_upgrade').execute('upgrade')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category 1')
    >>> category.save()
    >>> template = ProductTemplate()
    >>> product = Product()
    >>> template.name = 'Tray'
    >>> template.type = 'goods'
    >>> template.category = category
    >>> template.default_uom = unit
    >>> template.list_price = Decimal(10)
    >>> template.cost_price = Decimal(5)
    >>> template.occupy_space = True
    >>> template.save() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> template.length = Decimal(0.5)
    >>> meter, = ProductUom.find([('name', '=', 'Meter')], limit=1)
    >>> template.length_uom = meter
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Create locations::

    >>> Location = Model.get('stock.location')
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])
    >>> loc1 = Location(name='Location 1', parent=storage_loc,
    ...                 type='storage')
    >>> loc1.control_space = True
    >>> loc1.sequence = 1
    >>> loc1.length = Decimal(20)
    >>> loc1.length_uom = meter
    >>> loc1.overload_behavior = 'warn'
    >>> loc1.save()
    >>> loc2 = Location(name='Location 2', parent=storage_loc,
    ...                 type='storage')
    >>> loc2.control_space = True
    >>> loc2.sequence = 2
    >>> loc2.length = Decimal(20)
    >>> loc2.length_uom = meter
    >>> loc2.overload_behavior = 'warn'
    >>> loc2.save()
    >>> storage_loc.available_space
    40.0

Create moves::

    >>> StockMove = Model.get('stock.move')
    >>> move = StockMove()
    >>> move.product = product
    >>> move.uom =unit
    >>> move.quantity = 50
    >>> move.from_location = input_loc
    >>> move.to_location = loc1
    >>> move.company = company
    >>> move.planned_date = today
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> StockMove.click([move], 'distribute') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserWarning: ...
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='8@%s.check_storage_space' % str(today), always=True).save()
    >>> StockMove.click([move], 'distribute')
    >>> mov1, mov2 = StockMove.find(order=[('to_location', 'ASC')])
    >>> mov1.quantity
    40.0
    >>> mov2.quantity
    10.0
    >>> mov2.to_location.name
    u'Location 2'
