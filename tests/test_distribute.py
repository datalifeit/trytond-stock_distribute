# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import doctest_setup, doctest_teardown


class StockDistributeTestCase(ModuleTestCase):
    """Test stock distribute module"""
    module = 'stock_distribute'

    def setUp(self):
        super(StockDistributeTestCase, self).setUp()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(StockDistributeTestCase))
    suite.addTests(doctest.DocFileSuite('scenario_stock_distribute.rst',
           setUp=doctest_setup, tearDown=doctest_teardown, encoding='utf-8',
           optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    # TODO: check auto setting of sequence in locations
    # TODO: recreate a more complex scenario with different types of locations in the hierarchy, both products that
    # fill space and not, moves in different dates, control by surface, etc.
    return suite
