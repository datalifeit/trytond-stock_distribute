from trytond.pool import Pool
from .stock import *


def register():
    Pool.register(
        Move,
        Location,
        module='stock_distribute', type_='model')